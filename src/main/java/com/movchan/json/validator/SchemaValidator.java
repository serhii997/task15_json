package com.movchan.json.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class SchemaValidator {

    private static final Logger LOGGER = LogManager.getLogger(SchemaValidator.class);
    public void validate(String schemaPath, String jsonPath) throws IOException, ProcessingException {
        JsonNode date = JsonLoader.fromFile(new File(jsonPath));
        JsonNode schema = JsonLoader.fromFile(new File(schemaPath));

        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonValidator validator = factory.getValidator();
        ProcessingReport report = validator.validate(schema, date);

        LOGGER.info("json valid");
    }
}
