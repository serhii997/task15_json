package com.movchan.json.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movchan.model.Knife;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GsonParser {
    private static final Logger LOGGER = LogManager.getLogger(GsonParser.class);

    public void saveObjectToJson(List<Knife> gems, String jsonPath){
        try (Writer writer = new FileWriter(jsonPath)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(gems, writer);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public List<Knife> parseToObject(String jsonPath) {
        Gson gson = new Gson();
        List<Knife> knives = null;
        try (FileReader json = new FileReader(jsonPath)) {
            knives = Arrays.asList(gson.fromJson(json, Knife[].class));
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return knives;
    }

    public List<Knife> sortByOrigin(List<Knife> gem) {
        return gem.stream().sorted(Comparator.comparing(Knife::getOrigin)).collect(Collectors.toList());
    }
}
