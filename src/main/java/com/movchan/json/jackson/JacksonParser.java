package com.movchan.json.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.movchan.model.Knife;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class JacksonParser {
    private static final Logger LOGGER = LogManager.getLogger(JacksonParser.class);
    private ObjectMapper mapper = new ObjectMapper();

    public List<Knife> parseToObject(String jsonPath) {
        List<Knife> gems = null;
        try (FileReader json = new FileReader(jsonPath)) {
            gems = Arrays.asList(mapper.readValue(json, Knife[].class));
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
        return gems;
    }

    public List<Knife> sortByCarat(List<Knife> gem) {
        return gem.stream().sorted(Comparator.comparing(Knife::getOrigin)).collect(Collectors.toList());
    }

    public void saveObjectToJson(List<Knife> gems, String jsonPath) {
        try (Writer writer = new FileWriter(jsonPath)) {
            String arrayToJson = mapper.writeValueAsString(gems);
            writer.write(arrayToJson);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
