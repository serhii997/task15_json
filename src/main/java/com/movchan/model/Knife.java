package com.movchan.model;

import java.util.Objects;

public class Knife {
    private Integer id;
    private String type;
    private String handy;
    private String origin;
    private Visual visual;
    private boolean value;

    public Knife() {
    }

    public Knife(Integer id, String type, String handy, String origin, Visual visual, boolean value) {
        this.id = id;
        this.type = type;
        this.handy = handy;
        this.origin = origin;
        this.visual = visual;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Visual getVisual() {
        return visual;
    }

    public void setVisual(Visual visual) {
        this.visual = visual;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Knife knife = (Knife) o;
        return value == knife.value &&
                Objects.equals(id, knife.id) &&
                Objects.equals(type, knife.type) &&
                Objects.equals(handy, knife.handy) &&
                Objects.equals(origin, knife.origin) &&
                Objects.equals(visual, knife.visual);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, handy, origin, visual, value);
    }

    @Override
    public String toString() {
        return "Knife{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", handy='" + handy + '\'' +
                ", origin='" + origin + '\'' +
                ", visual=" + visual +
                ", value=" + value +
                '}';
    }
}

