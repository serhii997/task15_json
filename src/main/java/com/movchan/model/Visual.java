package com.movchan.model;

public class Visual {
    private Integer length;
    private Integer width;
    private String material;
    private String handle;
    private boolean bloodFlow;

    public Visual() {
    }

    public Visual(Integer length, Integer width, String material, String handle, boolean bloodFlow) {
        this.length = length;
        this.width = width;
        this.material = material;
        this.handle = handle;
        this.bloodFlow = bloodFlow;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public boolean isBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(boolean bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    @Override
    public String toString() {
        return "Visual{" +
                "length=" + length +
                ", width=" + width +
                ", material='" + material + '\'' +
                ", handle='" + handle + '\'' +
                ", bloodFlow=" + bloodFlow +
                '}';
    }
}
