package com.movchan.controller;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.movchan.Application;
import com.movchan.json.gson.GsonParser;
import com.movchan.json.jackson.JacksonParser;
import com.movchan.json.validator.SchemaValidator;
import com.movchan.model.Knife;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class ControllerImp implements Controller {

    private static final Logger LOGGER = LogManager.getLogger(Application.class);
    private static final String SCHEMA_PATH = "src/main/resources/knifesSchema.json";
    private static final String JSON_PATH = "src/main/resources/knife.json";

    @Override
    public void gsonParser() {
        try {
            new SchemaValidator().validate(SCHEMA_PATH, JSON_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ProcessingException e) {
            e.printStackTrace();
        }

        GsonParser gsonParser = new GsonParser();
        List<Knife> knives = gsonParser.parseToObject(JSON_PATH);
        knives = gsonParser.sortByOrigin(knives);
        knives.forEach(LOGGER::info);
        gsonParser.saveObjectToJson(knives, JSON_PATH);
    }

    @Override
    public void jacksonParser() {
        try {
            new SchemaValidator().validate(SCHEMA_PATH, JSON_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ProcessingException e) {
            e.printStackTrace();
        }
        JacksonParser jacksonParser = new JacksonParser();
        List<Knife> knives1 = jacksonParser.parseToObject(JSON_PATH);
        knives1 = jacksonParser.sortByCarat(knives1);
        knives1.forEach(LOGGER::info);
        jacksonParser.saveObjectToJson(knives1, JSON_PATH);
    }
}
