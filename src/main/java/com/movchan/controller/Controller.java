package com.movchan.controller;

public interface Controller {
    void gsonParser();
    void jacksonParser();
}
